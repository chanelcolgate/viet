const  http = require('http');
var app = require('./app');


// create port
const port = process.env.PORT || 1111;
// create server
 const server = http.createServer(app.ExApp);
 // listen port
 server.listen(port);
